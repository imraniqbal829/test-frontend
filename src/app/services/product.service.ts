import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }

  all(limit) {
    let params = new HttpParams({fromObject: {
      limit
    }});
    return this.http.get(this.url + 'product', { params })
    .pipe(map(data => {
      return data;
    }),
    catchError(err => of(false))
    )
  }


  get(id: string) {
    return this.http.get(this.url + 'product/' + id)
    .pipe(map(data => {
      return data;
    }),
    catchError(err => of(false))
    )
  }
}
