import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  response: any;
  backupProducts = [];
  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.all(12).subscribe(response => {
      console.log(response);
      this.response = response;
      // @ts-ignore
      this.backupProducts = response.data;
    });
  }

  onSearch(event) {
    let searchVal = event.target.value.toUpperCase();
    
    this.response.data = this.backupProducts.filter(product => {
      return product.name.toUpperCase().indexOf(searchVal) > -1;
    });
  }

}
