﻿# Setup your project

First **clone** the repository.

    git clone https://imraniqbal829@bitbucket.org/imraniqbal829/test-frontend.git

## Install Your project
It will **install** your project dependencies.

    npm install

## Start your project

It will **start** your project

    ng serve --open
Now just hit this link to view your project

[[http://localhost:4200/]](http://localhost:3000/)

>Note port 4200 is the default port in which it will your project in case you haven't changed your port. 

