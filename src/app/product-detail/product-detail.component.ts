import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  constructor(
      private route: ActivatedRoute,
      private productService: ProductService
    ) { }

  product: any;

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.productService.get(id).subscribe(res => {
      console.log(res);
      // @ts-ignore
      if(res.status) {
        // @ts-ignore
        this.product = res.data;
      }
    });
  }

}
